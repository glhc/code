function makeAnagram (a, b) {
  let deletions = 0;
  let aFreq = new Map();
  let bFreq = new Map();

  function populateMap (currentString, currentMap) {
    for (let i of currentString) {
      if (!currentMap.has(i)) {
        currentMap.set(i, 1);
      } else {
        currentMap.set(i, currentMap.get(i) + 1);
      }
    }
  }

  function findUniqueChars (mapOne, mapTwo) {
    let uniques = 0;    

    for (let [key, value] of mapOne) {
      if (!mapTwo.has(key)) {
        uniques += value;
      } else if (value > mapTwo.get(key)) {
        uniques += (value - mapTwo.get(key));
      }
    }

    return uniques;
  }

  populateMap(a, aFreq);
  populateMap(b, bFreq);
  deletions += findUniqueChars(aFreq, bFreq);
  deletions += findUniqueChars(bFreq, aFreq);


  return deletions;
}