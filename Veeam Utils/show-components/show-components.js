/**
 * This app will accept a log (text) file as input.
 * It will parse the log file for components. e.g.:
 * [27.05.2019 04:35:32] <75> Info     [WaService]	[27.05.2019 04:35:32] <75> Info     [WaService]
 * Allow you to select which components you would like to view
 * Possibly in format:
 * [ ] WaService
 * [x] Statistic
 * 
 * It will then print only the lines from the component selected to a new log file.
 *
 * After completion:
 * Should start by working on Svc.VeeamBackup.log, job.log and task.log, then try to work on others if possible
 * 
 * It should identify the backup server by existence of Svc.VeeamBackup.log
 * It should identify which jobs exist
 * It should then offer which job to choose (or Svc.VeeamBackup.log)
 * it should then offer task log or job log or 
 */



'use strict';

// Modules
const fs = require('fs');
const util = require('util');
const readline = require('readline');


// Constants
const fileLocation = process.argv[2];

main();


async function main () {
  if (!fileLocation) {
      process.stdout.write('No argument was given, exiting..\n');
      process.exit;
  }

  if (checkFileValidity(fileLocation)) {
    const stream = fs.createReadStream(); // <-- logic here!
  }
  await indexLogFile();
  await findComponents();
  await askUserChoices();
  await createOutput();
  await askUserChoices();
  await process.exit();
}

// Turn logfile takes a filename argument and returns a verified absolute file reference (possibly a file descriptor?)
async function checkFileValidity(filePath) {


  return absoluteFilePath;
}

// This function will take a log file (string/file/stream?) and return an indexed array of lines
async function indexLogFile() {

}