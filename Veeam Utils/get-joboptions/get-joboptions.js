/**
 * This util should print job options in a human readable format. Should work for backup job and backup copy job.
 * Util will offer a list of jobs, then a list of available times in the log bundle for job options
 * Util should display job options in a key/value format (maybe? that might not work) for the chosen time.
 */