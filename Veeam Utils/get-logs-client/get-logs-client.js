'use strict';

/**
 * This code will perform the following operations:
 * 1. Accepts a case number as an argument. Must be a valid case number.
 * 2. Access the S3 bucket subdirectory associated with that case number and download all files
 * 3. Access the FTP (if it exists) associated with that case number and download all files
 * 4. Extract archived files
 * Caveats:
 *  a. Logs will be downloaded to C:\Logs\<case_number>
 */
