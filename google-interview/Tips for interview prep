1. be able to prove what's on your resume
hashtables
stacks
arrays
space and time complexity
algorithms

2. testing



always explain what I'm thinking, what ideas I'm discarding

Questions are intentionally vague, always, always ask questions
e.g. are there any time/space complexity requirements?



Here's our advice to help you be ready for your interview.

    Predict the future: You can anticipate 90% of the interview questions you’re going to get. “Why do you want this job?” “What’s a tough problem you’ve solved?” If you can’t think of any, Google “most common interview questions.” Write down the top 20 questions you think you’ll get.

    Plan: For every question on your list, write down your answer. That will help them stick in your brain, which is important because you want your answers to be automatic.

    Have a backup plan: Actually, for every question, write down THREE answers. Why three? You need to have a different, equally good answer for every question because the first interviewer might not like your story. You want the next interviewer to hear a different story and become your advocate.

    Explain: We want to understand how you think, so explain your thought process and decision making throughout the interview. Remember we’re not only evaluating your technical ability, but also how you approach problems and try to solve them. Explicitly state and check assumptions with your interviewer to ensure they are reasonable.

    Be data-driven: Every question should be answered with a story that demonstrates you can do what you’re being asked about. “How do you lead?” should be answered with “I’m a collaborative/decisive/whatever leader. Let me tell you about the time I … ”

    Clarify: Many of the questions will be deliberately open-ended to provide insight into what categories and information you value within the technological puzzle. We’re looking to see how you engage with the problem and your primary method for solving it. Be sure to talk through your thought process and feel free to ask specific questions if you need clarification.

    Improve: Think about ways to improve the solution you present. It’s worthwhile to think out loud about your initial thoughts to a question. In many cases, your first answer may need some refining and further explanation. If necessary, start with the brute force solution and improve on it — just let the interviewer know that's what you're doing and why.

    Practice: Everyone gets better with practice. Practice your interview answers—out loud—until you can tell each story clearly and concisely.

