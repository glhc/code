'use strict';

const fs = require('fs');

let text = '';
let reg = new RegExp(/(?:https?:\/\/)(?:www\.)?(?<domain>(\w+\.?)+)/);

process.stdin.setEncoding('utf8');

process.stdin.on('data', inputStream => {
  text += inputStream;
});

process.stdin.on('end', async () => {
  await main();
});

// accept array of lines
async function findUrls (arr) {
  let domainHits = new Map(); // map for storing domains and hits

  for (let currentString of arr) {
    let results = await currentString.match(/(?:https?:\/\/)(?<domain>(www\.)?(\w+\.?)+)/);
    let domainAddress = await results.domain;

      if(domainAddress) {
        if (!domainHits.has(domainAddress)) {
          domainHits.set(domainAddress, 1);
        } else {
          domainHits.set(domainAddress, domainHits.get(domainAddress) + 1); // increment value
        }
      } 
  }

  return domainHits;
}


 async function main() {

  let result = await findUrls(text);
  
  for (let i of result.entries) { 
     console.log(result.entries[0] + ' ' + result.entries[1]);
  }

}

/*
Follow up: how well does your solution scale for a file with 100 lines, 100,000 lines or 10^9 lines of log entries?
*/