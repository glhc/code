
Now we want to understand more about your programming skill.  
Please have a look at the following pseudo-code.  Please note that the syntax may be completely different from any of programming languages you know.  More specifically, indentation does not matter in this code, "==" denotes the substitution, and "=" is the equal comparator.

========
Initial state of an array "a":

[[4, 2, 4, 2], 
 [4, NULL, 4, 2], 
 [2, NULL, 8, 2], 
 [16, NULL, 4, NULL]]

========
Main function=

FUNCTION foo()
    FOR y == 0 to 3 
        FOR x == 0 to 3
            IF a[x+1][y] != NULL // if next one is not empty
                IF a[x+1][y] = a[x][y] // if next one is the same as this one
                    a[x][y] == a[x][y]*2 // double this one
                    a[x+1][y] == NULL // set the next one to empty
                END IF
                IF a[x][y] = NULL // if this one is empty
                    a[x][y] == a[x+1][y] // set this one to the next one
                    a[x+1][y] == NULL // set the next one to empty
                END IF
            END IF
        END FOR
   END FOR
END FUNCTION

What is the issue with the above code? *
the a[x + 1] lookup exceeds the boundaries of the array when x = 3

How would you fix it? *
Any modification to the array as a result of this function is dependent on a[x + 1] having a value. Therefore, we can safely iterate from x = 0 to x = 2 to preserve the logic of the function without perfoming lookups of out-of-bounds values.


//don't forget about y?


[4, 2, 4, 2], 
[4, NULL, 4, 2], 
[2, NULL, 8, 2], 
[16, NULL, 4, NULL]



How could you make foo more generic? Explain up to three possible *generalization* directions and describe a strategy for each, no need to write the code! *

1. Change the upper limit of the iterations of x and y from the specific number, "3" to two independent variables, length "len" and width "wid". Use "len - 1" and "wid - 1" as upper limits to allow for processing of any array.

2. Introduce start and endpoint functionality. First, accept starting co-ordinates for x and y, "startX" and "startY". These values would be set as the lower boundaries on the x- and y- axes. This will allow any array to be processed from a non-origin starting point. Similarly, introduce distance values for x-axis, "distanceX" and the y-axis, "distanceY". Iteration would stop at "x + distanceX" or "y + distanceY". Error-checking logic would be required to prevent bad inputs.

3. Include compatibility with n-dimensional arrays by placing the current logic in a nested function, "bar". "foo" would be able to pass requisite dimensions into the nested function. This would allow one dimension to be modified with respect to another dimension as desired. Error-checking logic would be required to prevent bad inputs.

3. 

[4, 2, 4, 2], 
[4, 0, 4, 2], 
[2, 0, 8, 2], 
[16, 0, 4, 0]

[4, 2, 4, 2], 
[4, 4, 2, 0], 
[2, 0, 8, 2], 
[16, 0, 4, 0]


START

if the next one is full

THEN

if the next one is the same as this one:
    double this one
    make the next one empty

AND

if this one is empty
    make this one the same as the next one
    make the next one empty

REPEAT.


this one: full
next one: empty


it keeps checking past the end of the array






Which best describes your area of most experience? *
Web development: HTTP(S), how browsers work, APIs, authentication, cookies. Understanding the architecture of the web and how to optimize web applications. Basic SQL and relational database adminstration.
Big Data and Machine Learning: Relational and nonrelational databases, Big Data analytics and frameworks like MapReduce, Hadoop and Spark. Machine learning/artificial intelligence, like TensorFlow.
Infrastructure and system administration: Shell scripting, logging, initalization, software packaging and distribution. Kernel, libraries, system calls, memory management, permissions, file systems for Linux/Unix or Windows. Container technologies like Kubernetes and Docker.
Networking: TCP/IP, UDP, ICMP, IP packets, DNS, OSI layers, load balancing, static routing, BGP, OSPF in Linux/UNIX.