'use strict';

const fs = require('fs');
const util = require('util');

let inputData = '';
let filePath = process.argv[2];
let read = util.promisify(fs.readFile);
console.log("current working directory: " + process.cwd());
main();
// read, split the data then call main function.


// Prints the results
async function printResults(domains) {
  for (let [key, value] of domains) { // Iterate over the results
    process.stdout.write(`Domain: ${key} occurs ${value} times.\n`);
  }
}

// Finds the domain in a given string
async function findUrls(text) {
  let domainMap = new Map();
  let regex = new RegExp(/(?:https?:\/\/)(?<url>(?:(?:[a-zA-Z0-9-]+\.?)+))/);

  for (let line of text) {
    let domain = line.match(regex);

    // If the domain exists already, increment counter. Or else, add the domain.
    if (domain) {
      if (!domainMap.has(domain.groups.url)) {
        domainMap.set(domain.groups.url, 1);
      } else {
        domainMap.set(domain.groups.url, domainMap.get(domain.groups.url) + 1);
      }
    }
  }
  return domainMap; // returns a Map of the domains and how often they occur.
}


async function main () {
  if (!filePath) {
    process.stdout.write('Valid filepath must be given.\n');
    process.exit();
  } else {
    read(filePath, 'utf8')
      .then(data => {
        return data.split('\n');
      })
      .then(lines => {
        return findUrls(lines);
      })
      .then(domainFreq => {
        return printResults(domainFreq);
      })
      .then( () => {
        process.exit();
      })
      .catch(error => {
        console.log(error);
      });
  }
}