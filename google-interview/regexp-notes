Character Classes:

	.
	matches any single character except line terminators: \n, \r
	inside a character set, the dot loses its special meaning and matches a literal dot.

	\d
		matches any digit e.g. [0-9]

	\D
		Matches any character which is not a digit, e.g. [^0-9]

	\w
		Matches any alphanumeric character from the basic Latin alphabet, including the underscore.
		Equivalent to [A-Za-z0-9_]

	\W
		Matches any character that is not an alphanumeric character from the basic Lating alphabet.
		Equivalent to [^A-Za-z0-9_]

	\s
		Matches a single whitespace character

	\S
		Matches a single character other than whitespace

	\t
		matches a tab

	\n
		Matches a linefeed (newline)

	\r
		matches a carriage return

	[\b]
		matches a backspace

	\0
		Matches NULL

	\
		Indicates that the next character should not be interpreted literally and has a special significance when the character has a literal value.



Character Sets:

	[xyz]
		A charcter set. Matches any one of the enclosed characters.
		A range can be specified using hyphens.
		If the hyphen is at the start or end fo the set, then the hyphen is taken literally.

	[^xyz]
		Negated character set.
		Matches anything that is not enclosed in the brackets.

Alternation:

	x | y
		Matches either x or y
		/green|red/ will match "green" in "green apple" or "red" in "red apple"

Boundaries:

	^
		Matches start of input.
		If the multiline flag is set to true, also matches immediately after a line break character.

	$
		Matches end of input. 
		If the multiline flag is set to true, also matches immediately before a line break character.

	\b
		Matches a word boundary. This is an invisible position directly before or after a word
		Between the first/last letter and the space.

	\B
		matches a non-word boundary.
		Invisible point between two characters

Grouping and Back References:

	(x)
		Matches x and remembers the match. These are called capturing groups.
		e.g. /(foo)/ matches and remembers "foo" in "foo bar"
		The capturing groups are numbered according to the order of left parenthesis of capturing groups starting from 1.
		The matched substring can be recalled from the resulting array's elements[1], ..., [n] or from the predefined RegExp object's properties $1, ..., $9

	\x
		Where x is a positive integer. A back reference to the most recent substring matching the n parenthetical in the regular expression
		e.g. /apple(,)\sorange\1/ matches "apple, orange," in "apple, orange, cherry, peach"

	(?:x)
		Matches x but does not remember the match. These are called non-capturing groups. The matched substring cannot be recalled from the resulting arra's elements [1], ..., [n] or from the predefined RegExp object's properties $1, ..., $9

Quantifiers

	x*
		Matches the preceding item 0 or more times
		e.g. /bo*/ matches "booooooo" in "a ghost boooooooed". and "b" in "a bird warbled".

	x+
		Matches the preceding item 1 or more times

	x?
		Matches the preceding item 0 or 1 times

	x{n}
		Matches the preceding occurance x exactly n times.

	x{n,}
		Matches the preceding occurance x at least n times.

	x{n, m}
		Matches between n to m occurances of the preceding occurance x

	x*?
	x+?
	x??
	x{n}?
	x{n, m}
		Matches the preceding item x as above, hoever the match is the smallest possible match (instead of the default largest match)

Assertions:

	x(?=y)
		Matches x only if x is followed by y

	x(?!y)
		Matches x only if x is not followed by y