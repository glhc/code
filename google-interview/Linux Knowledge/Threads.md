# ICMP

The Internet Contorl Message Protocal is part of the TCP/IP protocol suite. it helps with debugging network issues.

Each ICMP message contains a type, code and checksum field.

###### Type
Type of ICMP message
###### Code
Sub-type which gives more information about the message
###### Checksum
Checksum is used to detect any issues with the intergrity of the message.

#### Common ICMP Types
- Type 0 - Echo Reply
- Type 3 - Destination unreachable
- Type 11 - Time excheeded

Within type 3 there are 16 code values that will further describe why it can't get to the destination:
- Code 0 - Network unreachable
- Code 1 - Host unreachable