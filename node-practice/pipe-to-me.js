'use strict'


let inputString = '';

process.stdin.on('data', inputStream => {
	inputString += inputStream;
});

process.stdin.on('end', () => {
	main();
});

function main () { // send cached stdin data to stdout
	process.stdout.write(inputString);
}
