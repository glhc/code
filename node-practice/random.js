"use strict";

let arr = [0, 1, "two"];

console.log("type of arr: " + typeof(arr));
console.log("type of arr[0]: " + typeof(arr[0]));
console.log("type of arr[2]: " + typeof(arr[2]));
